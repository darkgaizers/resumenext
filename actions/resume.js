import { GET_RESUME_EXP, GET_RESUME } from './'

export function getResume(user) {
	return {
		type: GET_RESUME,
		user
	}
}

export function getResumeExp(user) {
	return {
		type: GET_RESUME_EXP,
		user
	}
}
