import React from 'react'

import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import Divider from 'material-ui/Divider';
import Grid from 'material-ui/Grid';
  const style = {padding: 15};
  const styles = theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  });

const ExpItem = ({ classes,content }) => (
        <div>
            
            <h5>{content.company}</h5>
            <div style={{style}}>
            <Grid container spacing={24}>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>xs=12 sm=6</Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>xs=12 sm=6</Paper>
        </Grid>
      </Grid>
            {content.description}
            </div>
            <br/>
            <Divider/>
        </div>
)

export default withStyles(styles)(ExpItem);
