import React from 'react'
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Avatar from 'material-ui/Avatar';
import FileFolder from 'material-ui-icons/Folder';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import { withStyles } from 'material-ui/styles';
// import {
//     blue300,
//     indigo900,
//     orange200,
//     deepOrange300,
//     pink400,
//     purple500,
//   } from 'material-ui/styles/colors';
  const style = {margin: 10};
  const styles = {
    root: {
      flexGrow: 1,
      position:'sticky',
      top:0,
      zIndex:1000
    },
    flex: {
      flex: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    bar:{

    }
  };
  function HeaderBar(props) {
    const { classes } = props;
    return (
      <div className={classes.root}>
        <AppBar position="sticky" style={styles.bar}>
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="title" color="inherit" className={classes.flex}>
              My Resume (Next.js)
            </Typography>
            <Button color="inherit">Contact</Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
  
  HeaderBar.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(HeaderBar);
