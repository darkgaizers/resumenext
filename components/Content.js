import React from 'react'

const Content = ({ content }) => (
    <div
        title="Wisanu"
        iconClassNameRight="muidocs-icon-navigation-expand-more"
    >
    {
        content
    }
    </div>
)

export default Content
