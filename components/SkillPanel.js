import React from 'react'

import Paper from 'material-ui/Paper';

import ListItem from 'material-ui/List/ListItem';


import SkillChip from './SkillChip'
import Grid from 'material-ui/Grid'
import { Parallax, Background } from 'react-parallax';
const style = { margin: 5, padding: 5, backgroundImage: 'url(static/img/me.png)', minHeight: '45em', backgroundPosition: 'center top', backgroundRepeat: 'no-repeat' };
const SkillPanel = ({ content }) => (
    <div style={{ paddingTop: 20 }}>
    <h3 className="panel-header">Skills</h3>
                <Grid container spacing={24}>
                {
            content.map((c, idx) => {
                return <Grid key={idx} item xs={6} sm={3}>
                    <SkillChip content={c} />
                </Grid>
            })
        }
      </Grid>

  </div>
)

export default SkillPanel
