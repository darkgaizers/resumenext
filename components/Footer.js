import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import BottomNavigation, { BottomNavigationAction } from 'material-ui/BottomNavigation';
import IconBusiness from 'material-ui-icons/Business';
import IconInfo from 'material-ui-icons/AccountBox';
import IconBuild from 'material-ui-icons/Build';
const skillIcon = <IconBuild/>;
const infoIcon = <IconInfo/>;
const experienceIcon = <IconBusiness />;

/**
 * A simple example of `BottomNavigation`, with three labels and icons
 * provided. The selected `BottomNavigationItem` is determined by application
 * state (for instance, by the URL).
 */
class BottomNavigationExampleSimple extends Component {
  state = {
    selectedIndex: 0,
  };

  select = (index) => this.setState({selectedIndex: index});
  handleScrollToElement(target) {
    const tesNode = ReactDOM.findDOMNode(target)
    if (1){
      window.scrollTo(0, tesNode.offsetTop);
    }
  }
  render() {
    return (
        <BottomNavigation value={this.state.selectedIndex} className="footer">
          <BottomNavigationAction 
            label="Info"
            icon={infoIcon}
            onClick={() => this.select(0)}
          />
          <BottomNavigationAction 
            label="Experience"
            icon={experienceIcon}
            onClick={() => this.select(1)}
          />
          <BottomNavigationAction 
            label="Skills"
            icon={skillIcon}
            onClick={() => this.select(2)}
          />
        </BottomNavigation>
    );
  }
}

export default BottomNavigationExampleSimple;