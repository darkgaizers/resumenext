import React from 'react'

import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import Divider from 'material-ui/Divider';
import Grid from 'material-ui/Grid';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
  const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
      },
    chip: {
      margin: theme.spacing.unit,
    },
    close: {
        width: theme.spacing.unit * 4,
        height: theme.spacing.unit * 4,
      },
  });
  function handleDelete() {
    alert('You clicked the delete icon.'); // eslint-disable-line no-alert
  }
  
  function handleClick() {
    alert('You clicked the Chip.'); // eslint-disable-line no-alert
  }
  
const ExpItem = ({ classes,content }) => (
        <div>
            <Chip
        avatar={<Avatar>Tech</Avatar>}
        label={content}
        onClick={handleClick}
        className={classes.chip}
      />
        </div>
)

export default withStyles(styles)(ExpItem);
