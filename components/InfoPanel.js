import React from 'react'
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';

import List from 'material-ui/List/List';

import { Parallax, Background } from 'react-parallax';
const style = { margin: 5, padding: 5, backgroundImage: 'url(static/img/me.png)', minHeight: '45em', backgroundPosition: 'center top', backgroundRepeat: 'no-repeat' };
const InfoPanel = ({ content }) => (
  <div style={{ paddingTop:20}}>
      <h3 className="panel-header">Background</h3>
      <Paper style={style} >
    <div>
      I'm a System Analyst/Developer and IT Consultant who got experienced in
  - TV Broadcasting
  - Video on Demand
  - E-Commerce
  - Location-based software
  - Game Development
  - Others cross-functional tasks (Online Marketing, Product Owner, Business Analyst)
  - Project Management ( Agile)
  Currently leaning about coding the Smart Contract . interested in Blockchain and Cryptocurrency implementation.
  I constantly update my knowledge and improve my technical skills. I can work aboard.
  <span> Get in Touch -> <a href="mailto:darkgaizers@gmail.com">darkgaizers@gmail.com</a></span>
    </div>
  </Paper>
  </div>
)

export default InfoPanel
