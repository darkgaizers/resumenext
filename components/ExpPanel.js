import React from 'react'

import Paper from 'material-ui/Paper';
import ListItem from 'material-ui/List/ListItem';

import ExpItem from './ExpItem';

const style = { margin: 10, paddingTop: 20, paddingBottom: 20 };
const ExpPanel = ({ content }) => (
  <div>
    <h3 className="panel-header">Experience</h3>
    <Paper style={style}>

      {

        content.map((c, idx) => {

          return <ListItem key={c.id}><ExpItem  content={c} /></ListItem>
        })
      }
    </Paper>
  </div>

)

export default ExpPanel
