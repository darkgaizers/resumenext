import React from 'react'

import Paper from 'material-ui/Paper';

import FontIcon from 'material-ui/FontIcon';
import List from 'material-ui/List/List';
import { Parallax, Background } from 'react-parallax';
const style = { margin: 5, padding: 5, backgroundImage: 'url(https://lh3.googleusercontent.com/1TKQQuJGHC_OS0LI2jWH2kYyLz2C24QOISnOvK5Ggo41IRaqpBQssO-pyB6XhBPJYLt2BRtZ9YWGcaorSc_OrYA7QbsrNYQscI5FGCUm6lPScqJ1nshsmmW8XNsv1AVXFZwN_N9Y_Tjt8NsO8_FZTcidy2h7pzWRlot7i7WVFJqfx9AVovpnidqeSVQXtLp1j7OajPcgpuBDFrwOqC50RobpodVdrzelWTVL2MSh5VzHxv4lqweDrncQTyXDRXDReHaynH_cMQcrj7USTzkPBJrVUClB6vcGnG_Gf2KNyvXHyqLbXuJAFhwtaahUikMhgAPPYXY_ondKmFvhTFQ6enVASOAF80LvkoZh0PqGB6TMTaK17f7e1AEKgI04fxZljogct-VPV7SJFV7HLo5_Z7ganGTCUG488yH9LKJ8ppxSDE0GvzkkKNsGXsh52BKS-u2zNcNfCBiy4Jl2lUOADKNDBznvqtyzm-lCREdl_NnWFgfK-F44VEwzUgOMvUD9nOYLgMXn6zmGe-uIvnSr-IExTnnCNJKH0GSsQ5VS7iqSzNG5YjDo_zDwsRi-zVNsUbRDJDRVvGOjPaB4xUY8aKF_98TNyVCFNko3VgjG=w1732-h974-no)', minHeight: '45em', backgroundPosition: 'center top', backgroundRepeat: 'no-repeat' };
const InfoPanel = ({ content }) => (
    <Paper style={style} zDepth={1} rounded={false} >
        <div>
            <Parallax
                blur={{ min: -15, max: 15 }}
                bgImage={'https://lh3.googleusercontent.com/ArA1uj81I-AjSiabKDvv8x4Jmp1WqC8jZbC_iw-uSCUCDT8E_ctPPqckXpXVStrUpKjTbBNkU2c=w292-h437-no'}
                bgImageAlt="the dog"
                strength={200}
            >
                <Paper style={{ marginTop: '30em', height: '30em', 'backgroundColor': 'rgba(255,255,255,0.5)' }}>
                    <h1>Mr. Wisanu Choungsuwanish</h1>
                </Paper>
                <div style={{ height: '50em' }} />
            </Parallax>
            <br />
            <br />
            <Parallax
                blur={{ min: -15, max: 15 }}
                bgImage={'https://lh3.googleusercontent.com/1TKQQuJGHC_OS0LI2jWH2kYyLz2C24QOISnOvK5Ggo41IRaqpBQssO-pyB6XhBPJYLt2BRtZ9YWGcaorSc_OrYA7QbsrNYQscI5FGCUm6lPScqJ1nshsmmW8XNsv1AVXFZwN_N9Y_Tjt8NsO8_FZTcidy2h7pzWRlot7i7WVFJqfx9AVovpnidqeSVQXtLp1j7OajPcgpuBDFrwOqC50RobpodVdrzelWTVL2MSh5VzHxv4lqweDrncQTyXDRXDReHaynH_cMQcrj7USTzkPBJrVUClB6vcGnG_Gf2KNyvXHyqLbXuJAFhwtaahUikMhgAPPYXY_ondKmFvhTFQ6enVASOAF80LvkoZh0PqGB6TMTaK17f7e1AEKgI04fxZljogct-VPV7SJFV7HLo5_Z7ganGTCUG488yH9LKJ8ppxSDE0GvzkkKNsGXsh52BKS-u2zNcNfCBiy4Jl2lUOADKNDBznvqtyzm-lCREdl_NnWFgfK-F44VEwzUgOMvUD9nOYLgMXn6zmGe-uIvnSr-IExTnnCNJKH0GSsQ5VS7iqSzNG5YjDo_zDwsRi-zVNsUbRDJDRVvGOjPaB4xUY8aKF_98TNyVCFNko3VgjG=w1732-h974-no'}
                bgImageAlt="the dog"
                strength={200}
            >
                Blur transition from min to max
<div style={{ height: '50em' }} />
            </Parallax>
            <br />
            <br />
            <Parallax
                blur={{ min: -15, max: 15 }}
                bgImage={'https://scontent.fbkk2-4.fna.fbcdn.net/v/t1.0-9/26166692_1647558828599038_7081248355127210115_n.jpg?oh=2366a0fa49b1203fb56b864542ac2435&oe=5B4397CC'}
                bgImageAlt="the dog"
                strength={200}
            >
                Blur transition from min to max
<div style={{ height: '50em' }} />
            </Parallax>
            <br />
            <br />
            <Parallax
                blur={{ min: -15, max: 15 }}
                bgImage={'https://scontent.fbkk2-4.fna.fbcdn.net/v/t1.0-9/26166692_1647558828599038_7081248355127210115_n.jpg?oh=2366a0fa49b1203fb56b864542ac2435&oe=5B4397CC'}
                bgImageAlt="the dog"
                strength={200}
            >
                Blur transition from min to max
<div style={{ height: '50em' }} />
            </Parallax>
            <Parallax
                blur={{ min: -15, max: 15 }}
                bgImage={'https://scontent.fbkk2-4.fna.fbcdn.net/v/t1.0-9/26166692_1647558828599038_7081248355127210115_n.jpg?oh=2366a0fa49b1203fb56b864542ac2435&oe=5B4397CC'}
                bgImageAlt="the dog"
                strength={200}
            >
                Blur transition from min to max
<div style={{ height: '50em' }} />
            </Parallax>
        </div>
    </Paper>
)

export default InfoPanel
