import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
      },
    chip: {
      margin: theme.spacing.unit,
    },
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
});
function handleDelete() {
    alert('You clicked the delete icon.'); // eslint-disable-line no-alert
  }
  
  function handleClick() {
    alert('You clicked the Chip.'); // eslint-disable-line no-alert
  }
class SkillChip extends React.Component {
  state = {
    open: false,
  };

  handleClickSnackbar = () => {
    this.setState({ open: true });
  };

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
            <Chip
        avatar={<Avatar>Tech</Avatar>}
        label={this.props.content}
        onClick={this.handleClickSnackbar}
        className={classes.chip}
      />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.open}
          autoHideDuration={3000}
          onClose={this.handleClose}
          SnackbarContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">Note archived</span>}
          action={[
            <Button key="undo" color="secondary" size="small" onClick={this.handleClose}>
              UNDO
            </Button>,
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
      </div>
    );
  }
}

SkillChip.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SkillChip);