import { GET_RESUME, GET_RESUME_EXP } from '../actions'

export default function(state = [], action) {
	const { type, text, todo } = action

	switch (type) {
		case GET_RESUME:
			return [
				...state,
				{
					id: Math.random()
						.toString(36)
						.substring(2),
					text
				}
			]
		case GET_RESUME_EXP:
			return state.filter(i => i !== todo)
		default:
			return state
	}
}
