import { combineReducers } from 'redux'

import todos from './todos'
import resume from './resume'
export default combineReducers({ todos })
