import 'isomorphic-fetch'
import React from 'react'
import withRedux from 'next-redux-wrapper'

import Fork from '../components/Fork'
import Todo from '../components/Todo'
import Header from '../components/Header'
import Footer from '../components/Footer'
import initStore from '../utils/store'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
class IndexOld extends React.Component {
	static async getInitialProps({ store }) {
		// Adding a default/initialState can be done as follows:
		// store.dispatch({ type: 'ADD_TODO', text: 'It works!' });
		const res = await fetch(
			'https://api.github.com/repos/ooade/NextSimpleStarter'
		)
		const json = await res.json()
		
		return { stars: json.stargazers_count }
	}

	render() {
		const { stars } = this.props
		const { userAgent,children } = this.props
		const muiTheme = getMuiTheme(getMuiTheme({userAgent: userAgent}))
		return (
			<MuiThemeProvider   muiTheme={muiTheme} >
				<div>
					<Header >
					<Fork stars={stars} />
					</Header>

					<div style={{ paddingTop: "10em" }}>
						
						<Todo />
					</div>
					<Footer/>
				</div>
			</MuiThemeProvider>
		)
	}
}

export default withRedux(initStore)(IndexOld)
