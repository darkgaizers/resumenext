import 'isomorphic-fetch'
import React from 'react'
import withRedux from 'next-redux-wrapper'

import InfoPanel from '../components/InfoPanel'
import ExpPanel from '../components/ExpPanel'
import SkillPanel from '../components/SkillPanel'
import Header from '../components/Header'
import Footer from '../components/Footer'
import initStore from '../utils/store'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import purple from 'material-ui/colors/purple';
const theme = createMuiTheme({
    palette: {
      primary: { main: purple[500] }, // Purple and green play nicely together.
      secondary: { main: '#11cb5f' }, // This is just green.A700 as hex.
    },
    
  });
class Resume extends React.Component {
    static async getInitialProps({ store }) {
        // Adding a default/initialState can be done as follows:
        // store.dispatch({ type: 'ADD_TODO', text: 'It works!' });
        const res = await fetch(
            'http://localhost:8000/api/resume/wisanuc'
        )
        const json = await res.json()
        //console.log(json)
        return { resume: json }
    }

    render() {
        const { resume } = this.props
        const { userAgent, children } = this.props
        return (
            <MuiThemeProvider theme={theme}>
                <div>
                <Header position="sticky" style={{position:'static'}}/>
                    <div className="content"/>
                    <InfoPanel content={resume.info}/>
                    <ExpPanel content={resume.experience} />
                    <SkillPanel content={resume.skills} />
                    <Footer />
                </div>

            </MuiThemeProvider>
        )
    }
}

export default withRedux(initStore)(Resume)
